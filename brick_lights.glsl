[VertexShader]
//
// Vertex shader for procedural bricks
//
// Authors: Dave Baldwin, Steve Koren, Randi Rost
//          based on a shader by Darwyn Peachey
//
// Copyright (c) 2002-2004 3Dlabs Inc. Ltd. 
//
// See 3Dlabs-License.txt for license information
//

const float SpecularContribution = 0.3;
const float DiffuseContribution  = 1.0 - SpecularContribution;

varying float LightIntensity[4];
varying vec2  MCposition;
varying vec3 colorC[4];

uniform struct Light
{
uniform vec3 lightSource;
uniform vec3 lightDirection;	
uniform vec3 reflectVec;
uniform float diffuse;
uniform vec3 lightVec;
uniform vec3 viewVec;

uniform float ammount;

}_Light;

void main(void)
{

Light l[4]; 
	l[0].lightSource = vec3(-50.0, 0.0, 0.0);
	l[1].lightSource = vec3(0.0, -50.0, 0.0);
	l[2].lightSource = vec3(5.0, 5.0, 5.0);
	l[3].lightSource = vec3(50.0, -50.0, -50.0);

	colorC[0] = vec3(0.2, 0.0, 0.0);
	colorC[1] = vec3(0.0, 0.2, 0.0);
	colorC[2] = vec3(0.0, 0.0, 0.2);
	colorC[3] = vec3(0.2, 0.0, 0.2);		
		
    vec3 ecPosition = vec3 (gl_ModelViewMatrix * gl_Vertex);
    vec3 tnorm      = normalize(gl_NormalMatrix * gl_Normal);//N
	float spec[4];

	for (int i = 0; i < 4; i++)
	{
		spec[i]      = 0.0;
		l[i].lightDirection  = normalize(l[i].lightSource - ecPosition);// L
		l[i].reflectVec = reflect(l[i].lightDirection, tnorm); //R
		l[i].diffuse = max(dot(l[i].lightDirection, tnorm), 0.0);//this is correct

    	l[i].lightVec   = normalize(l[i].lightSource - ecPosition);    
   		l[i].viewVec    = normalize(-ecPosition);//V

		    if (l[i].diffuse > 0.0)
		    {
        		spec[i] = max(dot(l[i].reflectVec, l[i].viewVec), 0.0);//cos y
        		spec[i] = pow(spec[i], 160.0);
		    }
		    
		   LightIntensity[i]  =  l[i].diffuse + spec[i];
    MCposition      = gl_Vertex.xz;
           // gl_FrontColor += normalize(vec4 (colorC[2], 1.0));

    gl_Position     = ftransform();
	}


}
[FragmentShader]
//
// Fragment shader for procedural bricks
//
// Authors: Dave Baldwin, Steve Koren, Randi Rost
//          based on a shader by Darwyn Peachey
//
// Copyright (c) 2002-2004 3Dlabs Inc. Ltd.
//
// See 3Dlabs-License.txt for license information
//  

uniform vec3  BrickColor;
uniform vec3  MortarColor;
uniform vec2  BrickSize;
uniform vec2  BrickPct;

varying vec2  MCposition;
varying float LightIntensity[4];
varying vec3 colorC[4];

void main(void)
{
    vec3  color[4];
    vec2  position, useBrick;
    
    position = MCposition / BrickSize;

    if (fract(position.y * 0.5) > 0.5)
        position.x += 0.5;

    position = fract(position);

    useBrick = step(position, BrickPct);

	for (int i = 0; i < 4; i ++)
	{
    	//color[i]  = mix(MortarColor, BrickColor, useBrick.x * useBrick.y);
	    color[i] = colorC[i];
	    color[i] += LightIntensity[i];

        gl_FragColor += normalize(vec4 (color[i], 1.0));
    }

}
[Parameters]
vec2 BrickSize = vec2(0.3, 0.15);
