[VertexShader]
varying vec3 v_V;
varying vec3 v_N;
varying vec3 L;

uniform vec3 lightPos, lightPos2;


uniform struct Light
{
uniform vec3 lightSource;
uniform vec3 lightDirection;	
uniform vec3 reflectVec;
uniform float diffuse;
uniform vec3 lightVec;
uniform vec3 viewVec;
uniform float ammount;

}_Light;

Light l[2]; 
varying vec3 colorC[2];
varying float LightIntensity[2];

void main() {
	v_V = gl_Vertex.xyz;	
	v_N = gl_NormalMatrix * gl_Normal;


	l[0].lightSource = lightPos;
	l[1].lightSource = lightPos2;
	

	
	vec4 vEye = gl_ModelViewMatrix * gl_Vertex;
	float spec[2];
	L = vec3(0.0, 0.0, 0.0);

	for (int i = 0; i < 2; i++)
	{
		spec[i]    = 0.0;
		l[i].lightDirection  = normalize(l[i].lightSource - vEye.xyz);// L
		l[i].reflectVec = normalize(l[i].lightDirection + vec3(0.0, 0.0, 1.0)); //R
		l[i].diffuse = max(dot(l[i].lightDirection, v_N), 0.0);//this is correct

    	l[i].lightVec   = normalize(l[i].lightSource - vEye.xyz);    
   		l[i].viewVec    = normalize(v_N.xyz);//V

		    if (l[i].diffuse > 0.0)
		    {
        		spec[i] = max(dot(l[i].reflectVec, l[i].viewVec), 0.0);//cos y
        		spec[i] = pow(spec[i], 60.0);
		    }
		    
		   LightIntensity[i]  =  l[i].diffuse + spec[i];
	//l += LightIntensity[i];
	}
	
	//L = lightPos - vEye.xyz;
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;

}
[FragmentShader]
varying vec3 v_V;
varying vec3 v_N;
varying vec3 L;

uniform int uni;
uniform bool solid;
varying float LightIntensity[2];
uniform int object;

vec3 V;
vec3 surfaceColor;

void table(vec3 pos)
{
	if (((pos.y >= -1.0 && pos.y <= -0.8) || (pos.x >= -1.0 && pos.x <= -0.8)) ||
		((pos.y <= 1.0 && pos.y >= 0.8) || (pos.x <= 1.0 && pos.x >= 0.8)))
	{
		surfaceColor.r = (0.0);
		surfaceColor.g = (10.0);
		surfaceColor.b = (0.0);
	}
	else
	{
		surfaceColor.r = (0.0);
		surfaceColor.g = (2.0);
		surfaceColor.b = (0.0);	
	}
}
void stripedCueBall(vec3 pos)
{
	pos = normalize(pos);
	if (pos.y >= -0.7 && pos.y <= 0.7)
{
		surfaceColor.r = (10.0);
		surfaceColor.g = (0.0);
		surfaceColor.b = (0.0);
	}
	else
	{
		surfaceColor.r = (10.0);
		surfaceColor.g = (10.0);
		surfaceColor.b = (10.0);
	}	
}
void solidCueBall()
{
	surfaceColor.r = (10.0);
	surfaceColor.g = (0.0);
	surfaceColor.b = (0.0);
}

void main() {
	vec3 L = normalize(L);
	v_V;
//	vec3 L = normalize(vec3(gl_LightSource[0].position));


//	vec4 ambient = gl_FrontMaterial.ambient;
	//vec3 diffuse = 0.01 + vec3(max(dot(L, N), 0.1));
	//surfaceColor *= diffuse.xyz;
	//vec3 specular = 0.75 * vec3(pow(max(dot(N, H), 0.0), 80.0));
	//surfaceColor += specular.xyz+diffuse.xyz;

	//gl_FragColor = vec4(surfaceColor, 1.0);
	
	for (int i = 0; i < 2; i ++)
	{
		if (!solid)
		{
			stripedCueBall(v_V);
		}
		else if (solid)
		{
			solidCueBall();	
		}
		if (object == 3)
		{
			table(v_V);
		}
	
    	//color[i]  = mix(MortarColor, BrickColor, useBrick.x * useBrick.y);
	   surfaceColor += LightIntensity[i];
            gl_FragColor += normalize(vec4 (surfaceColor, 1.0));


    }
	    //surfaceColor += LightIntensity[0];
}
[Parameters]
vec3 lightPos = vec3(0, 0, 0);
vec3 lightPos2 = vec3(11, 11.5, -4.4);
int object = 3;
bool solid = false;
